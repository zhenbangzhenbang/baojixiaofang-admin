import request from '@/utils/request'

export function login(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/login', // '/vue-element-admin/user/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/info', // '/vue-element-admin/user/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/logout', // '/vue-element-admin/user/logout',
    method: 'post'
  })
}

export function getUserList() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/user/search`,
    method: 'get'
  })
}

export function getPartUserList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/authsys-user/getPartMember`,
    method: 'get',
    params: data
  })
}

export function assignUserRoles(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/user/update/roles`,
    method: 'put',
    data
  })
}
