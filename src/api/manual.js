import request from '@/utils/request'

// 上传党务手册文件  请求参数：使用elementui中的上传文件组件
// 返回filename fileUrl
export function uploadManual(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/manual/upload',
    method: 'post',
    data
  })
}

// 添加党务手册信息到数据库
export function insertManual(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/manual/insert',
    method: 'post',
    data
  })
}

// 删除党务手册文件  根据maId删除
export function deleteManualById(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/manual/delete/`,
    method: 'delete',
    params: { 'maId': data }
  })
}

// 获取党务手册文件列表 参数为 pageIndex ,pageSize
export function getManualList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/manual/getList`,
    method: 'get',
    params: data
  })
}
export function manualCategoryInsert(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/manualCategory-category/insert',
    method: 'POST',
    data
  })
}

// 删除视频模块
export function manualCategoryDelete(data) {

  return request({
  baseURL: 'https://bjxfdd.com',
  // baseURL: 'https://bjxfdd.com',
    url: '/manualCategory-category/delete',
    method: 'DELETE',
    params: data //
  })
}
// 视频模块列表
export function manualCategoryGetList(data) {

  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/manualCategory-category/getList',
    method: 'GET',
    params: data || {}
  })
}
