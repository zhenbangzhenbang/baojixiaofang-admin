import request from '@/utils/request'
export function newsCategoryInsert(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/news-category/insert',
    method: 'POST',
    data
  })
}

// 删除视频模块
export function newsCategoryDelete(data) {

  return request({
  baseURL: 'https://bjxfdd.com',
  // baseURL: 'https://bjxfdd.com',
    url: '/news-category/delete',
    method: 'DELETE',
    params: data //
  })
}
// 视频模块列表
export function newsCategoryGetList(data) {

  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/news-category/getList',
    method: 'GET',
    params: data || {}
  })
}

export function getnewsList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/news/getAll`,
    method: 'get',
    data
  })
}

export function deletenewsById(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/news/delete`,
    method: 'delete',
    params: { 'newsId': data }
  })
}
