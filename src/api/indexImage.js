import request from '@/utils/request'

// 上传首页图片
export function uploadIndexImg(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video/uploadindeximg',
    method: 'post',
    data
  })
}
// 设置首页图片

export function setIndexImg(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/news/insert',
    method: 'post',
    data
  })
}

// 首页图片获取接 口
export function getIndexImg() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/msg/img',
    method: 'get'
  })
}
