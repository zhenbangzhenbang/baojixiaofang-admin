import request from '@/utils/request'
// 添加用户信息到数据库
export function authsysUserInsert(data) {
  console.log('======添加的数据参数：')
  console.log(data)
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/authsys-user/insertUser',
    method: 'POST',
    data // name，serialNumber,phoneNumber
  })
}

// 删除用户
export function authsysUserDelete(data) {
  console.log(data)
  return request({

    url: 'https://bjxfdd.com/authsys-user/delete',
    method: 'DELETE',
    params: data // urId
  })
}
// 用户列表
export function authsysUserGetList(data) {
  console.log('=============getlist--data:')
  console.log(data)
  return request({

    url: 'https://bjxfdd.com/authsys-user/getPartMember',
    method: 'GET',
    params: data || {
      name: '',
      serialNumber: '',
      pageIndex: 1,
      pageSize: 10
    }
  })
}
