import request from '@/utils/request'

export function getNotChargeList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/charge/getNotChargeList',
    method: 'GET',
    params: data
  })
}

export function paymentHistoryByserialNumber(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/charge/getPaymentHistoryBySerialNumber',
    method: 'GET',
    params: { 'serialNumber': data }
  })
}

export function addPersonalChargeInfo(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/charge/addCharge',
    method: 'post',
    data
  })
}

export function deleteChargeInfo(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/charge/delete',
    method: 'delete',
    params: { 'chId': data }
  })
}
