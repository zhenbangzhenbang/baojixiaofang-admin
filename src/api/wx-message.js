import request from '@/utils/request'

export function sendMessage(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/msg/sendToAll',
    method: 'POST',
    data
  })
}
