import request from '@/utils/request'

export function getRoutes() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/routes',
    method: 'get'
  })
}

export function getOperateMenuRoleId(mnName) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/auth/search/${mnName}`,
    method: 'get'
  })
}

export function login(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/auth/logout',
    method: 'post'
  })
}
