import request from '@/utils/request'

export function uploadPanorama(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/panorama/upload',
    method: 'post',
    data
  })
}

// 添加党务手册信息到数据库
export function insertPanorama(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/panorama/insert',
    method: 'post',
    data
  })
}

// 删除党务手册文件  根据maId删除
export function deletePanoramaById(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/panorama/delete`,
    method: 'delete',
    params: { 'paId': data }
  })
}

// 获取党务手册文件列表 参数为 pageIndex ,pageSize
export function getPanoramaList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/panorama/getList`,
    method: 'get',
    data
  })
}

export function panoramaCategoryInsert(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/panorama-category/insert',
    method: 'POST',
    data
  })
}

// 删除视频模块
export function panoramaCategoryDelete(data) {

  return request({
  baseURL: 'https://bjxfdd.com',
  // baseURL: 'https://bjxfdd.com',
    url: '/panorama-category/delete',
    method: 'DELETE',
    params: data //
  })
}
// 视频模块列表
export function panoramaCategoryGetList(data) {

  return request({
    baseURL: 'https://bjxfdd.com',
    // baseURL: 'https://bjxfdd.com',
    url: '/panorama-category/getList',
    method: 'GET',
    params: data || {}
  })
}
