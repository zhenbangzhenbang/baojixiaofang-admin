import request from '@/utils/request'

export function videoCategoryInsert(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video-category/insert',
    method: 'POST',
    data
  })
}

// 删除视频模块
export function videoCategoryDelete(data) {
  console.log(data)
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video-category/delete',
    method: 'DELETE',
    params: data //
  })
}
// 视频模块列表
export function videoCategoryGetList(data) {
  console.log(data)
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video-category/getList',
    method: 'GET',
    params: data || {}
  })
}
// 添加视频文件到数据库
export function videoInsert(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video/insert',
    method: 'POST',
    data
  })
}

// 删除视频文件
export function videoDelete(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video/delete',
    method: 'delete',
    params: data
  })
}

// 视频文件列表
export function videoGetList(data) {
  const a = JSON.stringify(data)// eslint-disable-line no-unused-vars
  console.log('=======')
  console.log(data)
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video/getList',
    method: 'GET',
    params: data
  })
}

// 根据视频id获取视频内容列表
export function videoGetContentById(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/video/getVideoById',
    method: 'GET',
    params: data //
  })
}
