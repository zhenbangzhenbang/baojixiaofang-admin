import request from '@/utils/request'

export function getList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/person/list',
    method: 'GET'
  })
}

export function addAddressBook(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/person/save',
    method: 'post',
    data
  })
}

export function editAddressBook(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/person/edit',
    method: 'post',
    data
  })
}

export function deleteAddressBook (data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/person/delete',
    method: 'delete',
    params: { 'id': data }
  })
}
