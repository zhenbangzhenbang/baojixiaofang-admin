import request from '@/utils/request'

// 添加外部学习资源  data:liName   liUrl  liDesc
export function insertStudySource(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/link/insert',
    method: 'post',
    data
  })
}

// 根据liId删除外部资源
export function deleteStudySourceByliId(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/link/delete/',
    method: 'delete',
    params: { 'liId': data }

  })
}

// 获取外部学习资源列表  pageIndex ,pageSize。

export function getStudySourceList(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/link/getList',
    method: 'get',
    params: data
  })
}
