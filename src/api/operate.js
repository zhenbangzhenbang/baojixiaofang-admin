import request from '@/utils/request'

export function getOperateList() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/operate/search',
    method: 'get'
  })
}
