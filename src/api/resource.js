import request from '@/utils/request'

export function getResourceList() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/resource/search',
    method: 'get'
  })
}

export function getResource(id) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/resource/search/${id}`,
    method: 'get'
  })
}

export function addResource(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/resource/create`,
    method: 'post',
    data
  })
}

export function updateResource(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/resource/update`,
    method: 'put',
    data
  })
}

export function deleteResource(id) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/resource/delete/${id}`,
    method: 'delete'
  })
}
