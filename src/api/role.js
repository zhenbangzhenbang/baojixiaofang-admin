import request from '@/utils/request'

export function getRoles() {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/role/search',
    method: 'get'
  })
}

export function addRole(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: '/role/create',
    method: 'post',
    data
  })
}

export function updateRole(id, data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/role/update/${id}`,
    method: 'put',
    data
  })
}

export function updateRolePermssions(data) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/role/update/perms`,
    method: 'put',
    data
  })
}

export function deleteRole(id) {
  return request({
    baseURL: 'https://bjxfdd.com',
    url: `/role/delete/${id}`,
    method: 'delete'
  })
}
